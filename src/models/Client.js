const mongoose = require('mongoose');
const validator = require('validator');

// client
const clientSchema = mongoose.Schema(
  {
    firstName: {
      type: String,
      trim: true,
      required: true,
    },
    lastName: {
      type: String,
      trim: true,
      required: true,
    },
    adress: {
      streetNumber: {
        type: Number,
        trim: true,
        required: true,
      },
      streetAdress: {
        type: String,
        trim: true,
        required: true,
      },

      city: {
        type: String,
        trim: true,
        required: true,
      },
      country: {
        type: String,
        trim: true,
        required: true,
      },
      postalCode: {
        type: String,
        trim: true,
        required: true,
      },
    },
  },
  {
    timestamps: true,
  }
);

var Client = mongoose.model('Clients', clientSchema);

module.exports = Client;
