const express = require('express');
const userRouter = require('./routes/user');
const clientRouter = require('./routes/client');
const cors = require('cors');
const port = process.env.PORT;
require('./db/db');

const app = express();
app.use(cors());
app.use(express.json());

app.use('/users', userRouter);
app.use('/clients', clientRouter);

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
