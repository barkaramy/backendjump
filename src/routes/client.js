const express = require('express');
const Client = require('../models/Client');
const User = require('../models/User');
const auth = require('../middleware/auth');
const router = express.Router();

router.post('/', auth, async (req, res) => {
  // Create
  try {
    const client = new Client(req.body);
    await client.save();
    const user = await User.addClient(req.user._id, client);
    res.status(201).send({ client, user });
  } catch (error) {
    res.status(400).send(error);
  }
});

router.get('/all', auth, async (req, res) => {
  // Get all
  const user = req.user;
  const clients = await user.clients;
  res.status(201).send({ clients });
});

router.get('/:clientId', auth, async (req, res) => {
  // Details
  const user = req.user;
  let client = user.clients.filter((client) => {
    return client._id == req.params.clientId;
  });
  client = client[0];
  res.status(201).send({ client });
});

module.exports = router;
